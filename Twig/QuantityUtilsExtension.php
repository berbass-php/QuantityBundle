<?php

namespace Berbass\QuantityBundle\Twig;

use Berbass\QuantityBundle\Entity\MoneyData;
use Berbass\QuantityBundle\Services\Formatters\MoneyFormatter;
use Symfony\Component\Translation\TranslatorInterface;

class QuantityUtilsExtension extends \Twig_Extension
{
    /** @var MoneyFormatter */
    protected $moneyFormatter;

    /** 
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        MoneyFormatter $moneyFormatter,
        TranslatorInterface $translator
    )
    {
        $this->moneyFormatter = $moneyFormatter;
        $this->translator = $translator;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('get_money_spell_trans', array($this, 'getMoneySpellTrans'))
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('money_amount_part', array($this, 'moneyAmountPartFilter'))
        );
    }

    public function getMoneySpellTrans($currency, $base, $tail = '')
    {
        $test = 'spell_' . $currency;

        $trans = $this->translator->trans(
            $test,
            [
                '%base%' => $base,
                '%tail%' => $tail
            ]
        );

        return $trans != $test
            ? $trans
            : implode(' ', [$base, $this->translator->trans($currency), $tail])
        ; 
        // return $base . ' dinars et ' . $tail . ' millimes'; 
    }

    public function moneyAmountPartFilter(MoneyData $money, $part)
    {
        /** @var string */
        $transString = 'spell_' . $money->getUnit();

        /** @var float */
        $value = $this->moneyFormatter->asFloat($money->getPrice());

        /** @var integer */
        $base = floor($value);

        /** @var float */
        $tail = $value - $base;

        $tail *= pow(
            10,
            $this
                ->moneyFormatter
                ->getDecimalsForCurrency($money->getUnit())
        );

        $tail = number_format($tail);

        return $part == 'tail'
            ? $tail
            : (
                $part == 'base'
                ? $base
                : ''
            )
        ;
    }

    public function getName()
    {
        return 'symdrik_quantity_utils_extension';
    }
}