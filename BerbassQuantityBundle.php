<?php

namespace Berbass\QuantityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Berbass\QuantityBundle\DependencyInjection\MoneyPairManagerCompilerPass;
use Berbass\QuantityBundle\DependencyInjection\TbbcCurrencyTypeCompilerPass;
use Berbass\QuantityBundle\DependencyInjection\TbbcMoneyFormatterCompilerPass;
use Berbass\QuantityBundle\DependencyInjection\TbbcMoneyManagerCompilerPass;
use Berbass\QuantityBundle\DependencyInjection\TbbcMoneyTypeCompilerPass;

class SymdrikQuantityBundle extends Bundle
{
	public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new MoneyPairManagerCompilerPass());
        $container->addCompilerPass(new TbbcCurrencyTypeCompilerPass());
        $container->addCompilerPass(new TbbcMoneyFormatterCompilerPass());
        $container->addCompilerPass(new TbbcMoneyManagerCompilerPass());
        $container->addCompilerPass(new TbbcMoneyTypeCompilerPass());
        
    }
}
