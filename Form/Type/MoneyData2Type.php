<?php

namespace Berbass\QuantityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Berbass\QuantityBundle\Form\DataTransformer\MoneyDataToArrayTransformer;

class MoneyData2Type extends AbstractType
{
    const TYPE_NAME = 'symdrik_money_data_2_form';

    protected $container;
    protected $pairMananger;
    protected $QTHelper;
    protected $decimals;
    protected $scale;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->pairManager = $this->container->get('tbbc_money.pair_manager');
        $this->QTHelper = $this->container->get('symdrik_quantity.utility.quantity_type');
        $this->decimals = (int) $this->container->getParameter('tbbc_money.decimals');
        $this->scale= (int) $this->container->getParameter('money_field.value_scale');
      
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $QTHelper = $this->QTHelper;
        $builder
            ->add('value', 'text', [
            	'label' => 'symdrik.quantity.label.value'
            ])
            ->add('unit', 'symdrik_quantity_unit_form', [
                'quantity_type' => $QTHelper::MONEY
            ])
            ->addModelTransformer(
                new MoneyDataToArrayTransformer($this->decimals, $this->scale)
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return self::TYPE_NAME;
    }
}
