<?php

namespace Berbass\QuantityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Berbass\QuantityBundle\Form\DataTransformer\TbbcMoneyToArrayTransformer as MoneyToArrayTransformer;
use Tbbc\MoneyBundle\Form\Type\MoneyType as ParentType;

/**
 * Form type for the Money object.
 */
class TbbcMoneyType
    extends ParentType
{   
    /** @var  int */
    protected $decimals;
    /** @var  int */
    protected $scale;

    public function __construct(
        $decimals,$scale
    )
    {
        parent::__construct($decimals);
        $this->scale = (int)$scale;
    }
   
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $builder
            ->add('tbbc_amount', new TextType())
            ->add('tbbc_currency', $options['currency_type'])
            ->addModelTransformer(
                new MoneyToArrayTransformer($this->decimals,$this->scale)
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'currency_type' => 'tbbc_currency',
            ))
            ->setAllowedTypes(array(
                'currency_type' => array(
                    'string',
                    'Tbbc\MoneyBundle\Form\Type\CurrencyType',
                ),
            ))
        ;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'tbbc_money';
    }
}
