<?php

namespace Berbass\QuantityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Berbass\QuantityBundle\Utils\QuantityType;

class LengthDataType extends AbstractType
{
    const TYPE_NAME = 'symdrik_length_data_form';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    { 
      
        /** @var array */
        $customUnits = isset($options['custom_units']) ?
            $options['custom_units'] :
            []
        ;

        $builder
            ->add('value', 'number', [
                'label' => 'symdrik.quantity.label.value'
            ])
            ->add('unit', 'symdrik_quantity_unit_form', [
                'label' => 'symdrik.quantity.label.unit',
                'quantity_type' => QuantityType::LENGTH,
                'custom_units' =>  $customUnits
            ])
        ;

    }
    
   
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {  
        $resolver->setDefined('custom_units');

        $resolver->setAllowedTypes('custom_units', 'array');

        $resolver->setDefaults(array(
            'data_class' => 'Berbass\QuantityBundle\Entity\LengthData',
            'cascade_validation' => true,

           
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return self::TYPE_NAME;
    }
}
