<?php

namespace Berbass\QuantityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Berbass\QuantityBundle\Utils\QuantityType;

class TimeDataType extends AbstractType
{
    const TYPE_NAME = 'symdrik_time_data_form';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var array */
        $customUnits = isset($options['custom_units']) ?
            $options['custom_units'] :
            []
        ;

        $builder
            ->add('value', 'number', [
                'label' => 'symdrik.quantity.label.value'
            ])
            ->add('unit', 'symdrik_quantity_unit_form', [
            	'label' => 'symdrik.quantity.label.unit',
            	'quantity_type' => QuantityType::TIME,
                'custom_units' =>  $customUnits
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefined('custom_units');
        $resolver->setAllowedTypes('custom_units', 'array');

        $resolver->setDefaults(array(
            'data_class' => 'Berbass\QuantityBundle\Entity\TimeData',
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return self::TYPE_NAME;
    }
}
