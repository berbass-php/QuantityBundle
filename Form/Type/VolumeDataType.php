<?php

namespace Berbass\QuantityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Berbass\QuantityBundle\Utils\QuantityType;

class VolumeDataType extends AbstractType
{
    const TYPE_NAME = 'symdrik_volume_data_form';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value', 'number', [
                'label' => 'symdrik.quantity.label.value'
            ])
            ->add('unit', 'symdrik_quantity_unit_form', [
                'label' => 'symdrik.quantity.label.unit',
                'quantity_type' => QuantityType::VOLUME
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Berbass\QuantityBundle\Entity\VolumeData'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return self::TYPE_NAME;
    }
}
