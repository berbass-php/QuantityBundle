<?php

namespace Berbass\QuantityBundle\Form\Type;

use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use PhpUnitsOfMeasure\PhysicalQuantity\Time;
use PhpUnitsOfMeasure\PhysicalQuantity\Volume;
use Berbass\QuantityBundle\Model\PhysicalQuantity\Thing;
use Berbass\QuantityBundle\Utils\MoneyUtil;
use Berbass\QuantityBundle\Utils\QuantityType;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UnitType extends AbstractType
{
    const TYPE_NAME = 'symdrik_quantity_unit_form';

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('quantity_type');
        $resolver->setAllowedTypes('quantity_type', ['string']);

        $resolver->setDefined('custom_units');
        $resolver->setAllowedTypes('custom_units', 'array');

        $resolver->setNormalizer(
            'choices',
            function ($options) {

                /** @var boolean */
                $quantityGiven = !empty($options['quantity_type']);

                if (!empty($options['custom_units']) && $quantityGiven) {
                    
                    return array_intersect_key($options['custom_units'], $this->getUnits($options['quantity_type']));
                }

                return $quantityGiven ?
                    $this->getUnits($options['quantity_type']) :
                    []
                ;
            }
        );
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'choice';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return self::TYPE_NAME;
    }

    /**
     * Gets the units for the given quantity type.
     *
     * @param string $quantityType The quantity type
     *
     * @return array The units.
     */
    static function getUnits($quantityType)
    {
        $unitsDefinitions = [];

        switch ($quantityType) {

            case QuantityType::MASS :

                $unitsDefinitions = Mass::getUnitDefinitions();
                break;

            case QuantityType::VOLUME :

                $unitsDefinitions = Volume::getUnitDefinitions();
                break;

            case QuantityType::LENGTH :

                $unitsDefinitions = Length::getUnitDefinitions();
                break;

            case QuantityType::TIME :

                $unitsDefinitions = Time::getUnitDefinitions();
                break;

            case QuantityType::THING :

                $unitsDefinitions = Thing::getUnitDefinitions();
                break;

            case QuantityType::MONEY :

                return MoneyUtil::getCurrenciesList();
            
            default:
                break;
        }

        $units = array_map(
            function($unit) {
                return $unit->getName();
            },
            $unitsDefinitions
        );

        return array_combine($units, $units);
    }
}
