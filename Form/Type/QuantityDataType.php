<?php

namespace Berbass\QuantityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\NumberToLocalizedStringTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\MoneyToLocalizedStringTransformer;

use Symfony\Component\Validator\Constraints as Assert;

class QuantityDataType extends AbstractType
{
    const TYPE_NAME = 'symdrik_quantity_data_form';

    /** @var Container */
    protected $container;

    /** @var \Berbass\QuantityBundle\Utils\QuantityType */
    protected $QTHelper;

    /** @var int */
    protected $moneyDecimals;

    /** @var  MoneyToLocalizedStringTransformer */
    protected $moneyValTransformer;

    /** @var  NumberToLocalizedStringTransformer */
    protected $numberValTransformer;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->QTHelper = $this->container->get('symdrik_quantity.utility.quantity_type');

        $this->moneyDecimals = (int) $this->container->getParameter('tbbc_money.decimals');
        $this->moneyValTransformer = new MoneyToLocalizedStringTransformer(null, null, null, pow(10, $this->moneyDecimals));

        $this->numberValTransformer = new NumberToLocalizedStringTransformer();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', 'hidden')
            ->add('value', 'text', [
            	'label' => 'symdrik.quantity.label.value',
            ])
            ->add('unit', 'symdrik_quantity_unit_form', [
            	'label' => 'symdrik.quantity.label.unit',
                'quantity_type' => '',
                'constraints' => [
                    'notBlank' => new Assert\NotBlank()
                ]
            ])
        ;

        $builder->add('quantityType', 'choice', [
            'data' => '',
            'choices' => $this->container->get('symdrik_quantity.utility.quantity_type')->getTypesList()
        ]);

        $QTHelper = $this->QTHelper;

        $builder->addModelTransformer(new CallbackTransformer(
                function ($QDAsObj) use ($QTHelper) {

                    if (!$QDAsObj) {
                        
                        return [];
                    }

                    $value = $QDAsObj->getValue();

                    if ($QDAsObj::Q_TYPE == $QTHelper::MONEY) {
                        
                        $value = $this->moneyValTransformer->transform((int) $value);
                    } else {

                        $value = $this->numberValTransformer->transform($value);
                    }

                    return [
                        'id' => $QDAsObj->getId(),
                        'value' => $value,
                        'unit' => $QDAsObj->getUnit(),
                        'quantityType' => $QDAsObj::Q_TYPE
                    ];
                },
                function ($QDAsArray) use ($QTHelper) {

                    if (empty($QDAsArray)) {

                        return NULL;
                    }

                    $repo = $this->container->get('symdrik_quantity.utility.quantity_type')->getRepositoryFor($QDAsArray['quantityType']);

                    $entity = $repo && $QDAsArray['id'] ?
                        $repo->findOneBy(['id' => $QDAsArray['id']]) :
                        NULL
                    ;

                    $value = (string) $QDAsArray['value'];
                    $value = str_replace(" ", "", $value);
                    
                    if ($QDAsArray['quantityType'] == $QTHelper::MONEY) {

                        $value = $this->moneyValTransformer->reverseTransform($value);
                        $value = round($value);
                        $value = (int)$value;

                        $QDAsArray['value'] = $value;
                    }  else {

                        $QDAsArray['value'] = $this->numberValTransformer->reverseTransform($value);
                    }

                    if ($entity) {
                        
                        $entity->setValue($QDAsArray['value']);
                        $entity->setUnit($QDAsArray['unit']);

                        return $entity;
                    }

                    return $this->container->get('symdrik_quantity.utility.quantity_type')
                ->getDataInstanceFor($QDAsArray['quantityType'], $QDAsArray['value'], $QDAsArray['unit']);
                }
            ))
        ;

        $formModifier = function (FormInterface $form, $quantityType = '') {

            $form
                ->remove('unit')
                ->add('unit', 'symdrik_quantity_unit_form', [
                    'label' => 'symdrik.central.pricinginfo.unit.label',
                    'quantity_type' => $quantityType,
                    'constraints' => [
                        'notBlank' => new Assert\NotBlank()
                    ]
                ])
            ;
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {

                $form = $event->getForm();

                $formModifier($form, (string) $form->get('quantityType')->getData());
            }
        );

        $builder->get('quantityType')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {

                $quantityType = (string) $event->getForm()->getData();

                $formModifier($event->getForm()->getParent(), $quantityType);
            }
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return self::TYPE_NAME;
    }
}
