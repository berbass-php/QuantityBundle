<?php

namespace Berbass\QuantityBundle\Form\DataTransformer;

use Money\Money;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\DataTransformer\MoneyToLocalizedStringTransformer;
use Tbbc\MoneyBundle\Form\DataTransformer\MoneyToArrayTransformer;

/**
 * Transforms between a Money instance and an array.
 */
class TbbcMoneyToArrayTransformer extends MoneyToArrayTransformer
{
    /** @var  MoneyToLocalizedStringTransformer */
    protected $sfTransformer;

    /** @var  int */
    protected $decimals;
     /** @var  int */
    protected $scale;

    public function __construct($decimals = 2,$scale = 2)
    {
        $this->decimals = (int)$decimals;
        $this->scale=(int)$scale;
        $this->sfTransformer = new MoneyToLocalizedStringTransformer($this->scale, null, null, pow(10, $this->decimals));
    }

   
}
