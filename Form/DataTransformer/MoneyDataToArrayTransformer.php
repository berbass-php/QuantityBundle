<?php

namespace Berbass\QuantityBundle\Form\DataTransformer;

use Money\Money;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\DataTransformer\MoneyToLocalizedStringTransformer;

use Berbass\QuantityBundle\Entity\MoneyData;

/**
 * Transforms between a MoneyData instance and an array.
 */
class MoneyDataToArrayTransformer implements DataTransformerInterface
{
    /** @var  MoneyToLocalizedStringTransformer */
    protected $sfTransformer;

    /** @var  int */
    protected $decimals;
    /** @var  int */
    protected $scale;

    public function __construct($decimals = 2,$scale=2)
    {
        $this->decimals = (int) $decimals;
        $this->scale = (int) $scale;
        $this->sfTransformer = new MoneyToLocalizedStringTransformer($this->scale, null, null, pow(10, $this->decimals));
    }

    /**
     * @inheritdoc
     */
    public function transform($data)
    {
        if (null === $data) {
            return null;
        }

        $amount = $this->sfTransformer->transform((int) $data->getValue());

        return array(
            'value' => $amount,
            'unit' => $data->getUnit()
        );
    }

    /**
     * @inheritdoc
     */
    public function reverseTransform($data)
    {
        if (null === $data) {
            return null;
        }

        if (!is_array($data)) {
            throw new UnexpectedTypeException($data, 'array');
        }
        if (!isset($data['value']) || !isset($data['unit'])) {
            return null;
        }
        $amount = (string)$data['value'];
        $amount = str_replace(" ", "", $amount);
        $amount = $this->sfTransformer->reverseTransform($amount);
        $amount = round($amount);
        $amount = (int)$amount;

        return new MoneyData($amount, $data['unit']);
    }

}
