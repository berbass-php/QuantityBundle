<?php

namespace Berbass\QuantityBundle\Model\Money;

use Money\Money;
use Berbass\QuantityBundle\Utils\MoneyUtil;
use Tbbc\MoneyBundle\Pair\PairManager as BasePairManager;

class PairManager extends BasePairManager
{
    public function getCurrencyCodeList()
    {
        $out = empty($this->currencyCodeList) || $this->currencyCodeList[0] == '*' ?
        	array_keys(MoneyUtil::getCurrenciesList()) :
        	$this->currencyCodeList;

        return $out;
    }

    /**
     * @inheritdoc
     */
    public function saveRatio($currencyCode, $ratio)
    {
    	if ($ratio == 'N/A') {
    		return;
    	}

    	return parent::saveRatio($currencyCode, $ratio);
    }

    /**
     * @inheritdoc
     */
    public function convert(Money $amount, $currencyCode)
    {
        // to-do update this method to take he ratio history in account
        // i.e add a date argument to convert with that day's rate
        if (strtolower($amount->getCurrency()->getName()) == strtolower($currencyCode)) {
            
            return $amount;
        }

        return parent::convert($amount, $currencyCode);
    }
}