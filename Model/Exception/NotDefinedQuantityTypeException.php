<?php

namespace Berbass\QuantityBundle\Model\Exception;

class NotDefinedQuantityTypeException extends \Exception
{
	public function __construct()
	{
		$this->message = 'The type of this quantity data object is not defined; you have to set the constant "Q_TYPE" to a value not equivalent to false.'
	}

	public function __toString()
	{
		return $this->message;
	}
}