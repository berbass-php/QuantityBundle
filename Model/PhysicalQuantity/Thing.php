<?php

namespace Berbass\QuantityBundle\Model\PhysicalQuantity;

use PhpUnitsOfMeasure\AbstractPhysicalQuantity;
use PhpUnitsOfMeasure\UnitOfMeasure;
use PhpUnitsOfMeasure\HasSIUnitsTrait;

class Thing extends AbstractPhysicalQuantity
{
    use HasSIUnitsTrait;

    protected static $unitDefinitions;

    protected static function initialize()
    {
        // Things
        $thing = UnitOfMeasure::nativeUnitFactory('thing');
        $thing->addAlias('things');
        static::addUnit($thing);

        static::addMissingSIPrefixedUnits(
            $thing,
            1,
            '%pthing',
            [
                '%Pthing',
                '%Pthings',
            ]
        );
    }
}
