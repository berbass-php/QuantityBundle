<?php

namespace Berbass\QuantityBundle\Utils;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Berbass\QuantityBundle\Entity\LengthData;
use Berbass\QuantityBundle\Entity\MassData;
use Berbass\QuantityBundle\Entity\MoneyData;
use Berbass\QuantityBundle\Entity\ThingData;
use Berbass\QuantityBundle\Entity\TimeData;
use Berbass\QuantityBundle\Entity\VolumeData;

class QuantityType
{
	const MASS = 'MASS';

    const MONEY = 'MONEY';

    const THING = 'THING';

    const VOLUME = 'VOLUME';

    const LENGTH = 'LENGTH';

    const TIME = 'TIME';

    protected $entityManager;

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $em The default injected entity manager
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * Gets the quantity data instance for the given quantity type.
     *
     * @param string $qType The quantity type
     * @param string $value The value of the quantity
     * @param string $unit The unit of the quantity
     *
     * @return \Berbass\QuantityBundle\Entity\AbstractQuantityData The data instance for.
     */
    public static function getDataInstanceFor($qType, $value = NULL, $unit = NULL)
    {
        switch ($qType) {

            case self::MASS :
                return new MassData($value, $unit);

            case self::MONEY :
                return new MoneyData($value, $unit);

            case self::THING :
                return new ThingData($value, $unit);

            case self::VOLUME :
                return new VolumeData($value, $unit);

            case self::LENGTH :
                return new LengthData($value, $unit);

            case self::TIME :
                return new TimeData($value, $unit);

            default:
                return NULL;
        }
    }

    /**
     * Gets the entity repository instance for the suitable quantity data.
     *
     * @param string $qType The quantity type
     *
     * @return NULL|ObjectRepository The repository for.
     */
    public function getRepositoryFor($qType)
    {
        $entityName = '';

        switch ($qType) {

            case self::MASS :
                $entityName = 'BerbassQuantityBundle:MassData';
                break;

            case self::MONEY :
                $entityName = 'BerbassQuantityBundle:MoneyData';
                break;

            case self::THING :
                $entityName = 'BerbassQuantityBundle:ThingData';
                break;

            case self::VOLUME :
                $entityName = 'BerbassQuantityBundle:VolumeData';
                break;

            case self::LENGTH :
                $entityName = 'BerbassQuantityBundle:LengthData';
                break;

            case self::TIME :
                $entityName = 'BerbassQuantityBundle:TimeData';
                break;

            default:
                $entityName = '';
                break;
        }

        return $entityName ?
            $this->entityManager->getRepository($entityName) :
            NULL;
    }

    public function getTypesList()
    {
        return (new \ReflectionClass($this))->getConstants();
    }
}