<?php

namespace Berbass\QuantityBundle\Utils;

class MoneyUtil
{
    const CURRENCY_DEFAULT = 'EUR';
    
	public static function getCurrenciesList($locale = '')
    {
        $list = $locale ?
            \Symfony\Component\Intl\Intl::getCurrencyBundle()->getCurrencyNames($locale) :
            \Symfony\Component\Intl\Intl::getCurrencyBundle()->getCurrencyNames()
        ;

        array_walk($list, function(&$value, $key){
            $value = $key . ' - ' . $value;
        });

        if (class_exists('Money\Currency')) {

            $validCurrencies = require __DIR__.'/../../../../../vendor/mathiasverraes/money/lib/Money/currencies.php';

            $fList = array_intersect(array_flip($list), array_flip($validCurrencies));

            return array_flip($fList);
        }

        return $list;
    }
}