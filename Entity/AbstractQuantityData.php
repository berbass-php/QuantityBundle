<?php

namespace Berbass\QuantityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Berbass\QuantityBundle\Model\Exception\NotDefinedQuantityTypeException;
use Berbass\ToolboxBundle\Model\Traits\TimestampableEntityTrait;
use Berbass\ToolboxBundle\Utils\Utils;

/**
 * AbstractQuantityData
 *
 * @ORM\Table(name="quantity_data")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="quantity_type", type="string")
 * @ORM\DiscriminatorMap({"mass" = "MassData", "volume" = "VolumeData", "thing"="ThingData", "money"="MoneyData", "length"="LengthData", "time"="TimeData"})
 */
abstract class AbstractQuantityData
{
    use TimestampableEntityTrait;

    const Q_TYPE = '';

    public function __construct($value = NULL, $unit = NULL)
    {
        if (empty($unit)) {
            return;
        }

        $this->value = empty($value) ? 0 : $value;

        $this->unit = $unit;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", options={"default"=0, "scale"=3, "precision"=20})
     */
    protected $value;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string")
     */
    protected $unit;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get quantity type
     * @return string
     * @throws NotDefinedQuantityTypeException
     */
    public function getType()
    {
        if (!self::Q_TYPE) {
            
            throw new NotDefinedQuantityTypeException();
        }
        return self::Q_TYPE;
    }

    /**
     * Get the quantity object, which can perform operations on the actual type of quantity
     *
     * @return Object|mixed
     */
    public abstract function getQuantity();

    public function __toString()
    {
        if (!$this->unit) {
            return '';
        }

        return
            Utils::keepSignificantDigits($this->getValue())
            . ' '
            . $this->getUnit()
        ;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return AbstractQuantityData
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return (float) $this->value;
    }

    /**
     * Set unit
     *
     * @param string $unit
     *
     * @return AbstractQuantityData
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    public function isEmpty() {
        return empty($this->unit) || empty($this->value);
    }

    public function __clone()
    {
        $this->id = NULL;
    }

    /**
     * Invoked before the entity is saved or updated.
     *
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function checkValue()
    {
        if (is_null($this->value)) {
            
            $this->value = 0;
        }
    }
}
