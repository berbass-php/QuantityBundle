<?php

namespace Berbass\QuantityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use PhpUnitsOfMeasure\UnitOfMeasure;

use Berbass\QuantityBundle\Model\PhysicalQuantity\Thing;
use Berbass\QuantityBundle\Utils\QuantityType;

/**
 * ThingData
 *
 * @ORM\Table(name="thing_data")
 * @ORM\Entity
 */
class ThingData extends AbstractQuantityData
{
    const Q_TYPE = QuantityType::THING;

    /**
     * Get quantity instance
     *
     * @return Thing|NULL
     */
    public function getQuantity()
    {
        if (!$this->unit) {

            return NULL;
        }

        $knownThings = array_map(
            function($thing) {
                return $thing->getName();
            },
            Thing::getUnitDefinitions()
        );

        $thingName = UnitOfMeasure::linearUnitFactory('thing', 1);

        if (!in_array($this->unit, $knownThings
            )) {

            $thingName->addAlias($this->unit);

            Thing::addUnit($thingName);
        }

        if (!$this->value) {

            return new Thing(0, $this->unit);
        }
        
        return new Thing($this->value, $this->unit);
    }
}
