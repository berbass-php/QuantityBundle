<?php

namespace Berbass\QuantityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use PhpUnitsOfMeasure\PhysicalQuantity\Mass;

use Berbass\QuantityBundle\Utils\QuantityType;

/**
 * MassData
 *
 * @ORM\Table(name="mass_data")
 * @ORM\Entity
 */
class MassData extends AbstractQuantityData
{
    const Q_TYPE = QuantityType::MASS;
    const DEFAULT_UNIT = 'kg';

    public function __construct($value = NULL, $unit = NULL)
    {
        if (empty($unit)) {
            $this->unit = self::DEFAULT_UNIT;
        }

        parent::__construct($value, $unit);
    }

    /**
     * Get quantity instance
     *
     * @return Mass|NULL
     */
    public function getQuantity()
    {
        if (!$this->unit) {

            return NULL;
        }

        if (!$this->value) {

            return new Mass(0, $this->unit);
        }

        return new Mass($this->value, $this->unit);
    }
}
