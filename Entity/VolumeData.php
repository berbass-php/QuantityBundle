<?php

namespace Berbass\QuantityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use PhpUnitsOfMeasure\PhysicalQuantity\Volume;

use Berbass\QuantityBundle\Utils\QuantityType;

/**
 * VolumeData
 *
 * @ORM\Table(name="volume_data")
 * @ORM\Entity
 */
class VolumeData extends AbstractQuantityData
{
    const Q_TYPE = QuantityType::VOLUME;
    const DEFAULT_UNIT = 'm^3';

    public function __construct($value = NULL, $unit = NULL)
    {
        if (empty($unit)) {
            $this->unit = self::DEFAULT_UNIT;
        }

        parent::__construct($value, $unit);
    }

    /**
     * Get quantity instance
     *
     * @return Volume|NULL
     */
    public function getQuantity()
    {
        if (!$this->unit) {

            return NULL;
        }

        if (!$this->value) {

            return new Volume(0, $this->unit);
        }
        
        return new Volume($this->value, $this->unit);
    }
}
