<?php

namespace Berbass\QuantityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Money\Money;
use Money\Currency;

use Berbass\QuantityBundle\Utils\QuantityType;
use Berbass\QuantityBundle\Utils\MoneyUtil;

/**
 * MoneyData
 *
 * @ORM\Table(name="money_data")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class MoneyData extends AbstractQuantityData
{
    const Q_TYPE = QuantityType::MONEY;

    /**
     * @var decimal
     *
     * @ORM\Column(name="print_value", type="decimal", options={"default"=0, "scale"=3}, nullable=true)
     */
    protected $printValue;

    public function __construct($value = NULL, $unit = MoneyUtil::CURRENCY_DEFAULT)
    {
        if ($value INSTANCEOF Money) {
            
            $this->setPrice($value);
            return;
        }

        $this->value = empty($value) ? 0 : $value;
        
        if (empty($unit)) {
            
            $this->unit = MoneyUtil::CURRENCY_DEFAULT;
            return;
        }

        $this->unit = $unit;
    }

    public function generate($value = NULL, $unit = NULL)
    {
        if ($value === NULL) {
            
            $value = self::getValue();
        }

        if ($unit === NULL) {
            
            $unit = self::getUnit();
        }

        return new self($value, $unit);
    }

    /**
     * Get quantity instance
     *
     * @return Money|NULL
     */
    public function getQuantity()
    {
        if (!$this->unit) {

            return NULL;
        }

        if (!$this->value) {

            return new Money(0, new Currency($this->unit));
        }

        return new Money((int) $this->value, new Currency((string) $this->unit));
    }

    /**
     * Set price
     *
     * @param Money $price
     * @return MoneyData
     */
    public function setPrice(Money $price = NULL)
    {
        if (!$price) {
        
            $this->value = 0;
            $this->unit = MoneyUtil::CURRENCY_DEFAULT;

            return $this;
        }
        
        $this->value = $price->getAmount();
        $this->unit = $price->getCurrency()->getName();

        return $this;
    }

    /**
     * Get price
     *
     * @return Money
     */
    public function getPrice()
    {
        return $this->getQuantity();
    }

    public function __toString()
    {
        if (!$this->unit) {
            return '';
        }

        return $this->getValue() . ' ' . $this->getUnit() . ' | Note!!! This value is not formated yet, you may use a suitable formatter.';
    }

    public function getPrintValue()
    {
        return $this->printValue;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function handlePrepersist()
    {
        $this->printValue = ($this->getValue() / 1000);
    }

    public function greaterThan(MoneyData $other)
    {
        if ($this->getUnit() != $other->getUnit()) {
            
            throw new \Exception("Cannot greatThan!!!", 1);
        }
        return $this->getQuantity()
            ->greaterThan($other->getQuantity())
        ;
    }

    public function equals(MoneyData $other)
    {
        if ($this->getUnit() != $other->getUnit()) {
            
            throw new \Exception("Cannot equals!!!", 1);
        }
        return $this->getQuantity()
            ->equals($other->getQuantity())
        ;
    }

    public function lessThan(MoneyData $other)
    {
        if ($this->getUnit() != $other->getUnit()) {
            
            throw new \Exception("Cannot lessThan!!!", 1);
        }

        return $this->getQuantity()
            ->lessThan($other->getQuantity())
        ;
    }

    public function add(MoneyData $other)
    {
        if ($this->getUnit() != $other->getUnit()) {
            
            throw new \Exception("Cannot add!!!", 1);
        }

        $this->setPrice($this->getQuantity()->add($other->getQuantity()));

        return $this;
    }

    public function subtract(MoneyData $other)
    {
        if ($this->getUnit() != $other->getUnit()) {
            
            throw new \Exception("Cannot subtract!!!", 1);
        }
        $this->setPrice($this->getQuantity()->subtract($other->getQuantity()));

        return $this;
    }

    public function divide($divisor, $roundingMode = NULL)
    {
        /** @var Money */
        $newPrice = $roundingMode ?
            $this->getQuantity()->divide(floatval((string) $divisor), $roundingMode) :
            $this->getQuantity()->divide(floatval((string) $divisor))
        ;

        $this->setPrice($newPrice);

        return $this;
    }

    public function multiply($multiplier, $roundingMode = NULL)
    {
        /** @var Money */
        $newPrice = $roundingMode
            ? $this->getQuantity()->multiply(floatval((string) $multiplier), $roundingMode)
            : $this->getQuantity()->multiply(floatval((string) $multiplier))
        ;

        $this->setPrice($newPrice);

        return $this;
    }

    public function __clone()
    {
        $this->id = NULL;
        /** @var Money */
        $newPrice = new Money(
            $this->getPrice()->getAmount(),
            $this->getPrice()->getCurrency()
        );

        $this->setPrice($newPrice);
    }

    /**
     * Gets the currency.
     *
     * @return string The currency.
     */
    public function getCurrency()
    {
        return $this->getUnit();
    }

    public function setCurrency($value)
    {
        $this->setUnit($value);
        
        return $this;
    }
}
