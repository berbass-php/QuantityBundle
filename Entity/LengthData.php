<?php

namespace Berbass\QuantityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use PhpUnitsOfMeasure\PhysicalQuantity\Length;

use Berbass\QuantityBundle\Utils\QuantityType;

/**
 * LengthData
 *
 * @ORM\Table(name="length_data")
 * @ORM\Entity
 */
class LengthData extends AbstractQuantityData
{
    const Q_TYPE = QuantityType::LENGTH;

    const DEFAULT_UNIT = 'm';

    public function __construct($value = NULL, $unit = NULL)
    {
        if (empty($unit)) {
            $this->unit = self::DEFAULT_UNIT;
        }

        parent::__construct($value, $unit);
    }

    /**
     * Get quantity instance
     *
     * @return Length|NULL
     */
    public function getQuantity()
    {
        if (!$this->unit) {

            return NULL;
        }

        if (!$this->value) {

            return new Length(0, $this->unit);
        }

        return new Length($this->value, $this->unit);
    }
}
