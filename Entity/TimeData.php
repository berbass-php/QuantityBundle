<?php

namespace Berbass\QuantityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use PhpUnitsOfMeasure\PhysicalQuantity\Time;

use Berbass\QuantityBundle\Utils\QuantityType;

/**
 * TimeData
 *
 * @ORM\Table(name="time_data")
 * @ORM\Entity
 */
class TimeData extends AbstractQuantityData
{
    const Q_TYPE = QuantityType::TIME;

    const DEFAULT_UNIT = 's';

    const UNIT_TRANS_PREFIX = 'symdrik.quantity.time_unit.';

    public function __construct($value = NULL, $unit = NULL)
    {
        if (empty($unit)) {
            $this->unit = self::DEFAULT_UNIT;
        }

        parent::__construct($value, $unit);
    }

    /**
     * Get quantity instance
     *
     * @return Time|NULL
     */
    public function getQuantity()
    {
        if (!$this->unit) {

            return NULL;
        }

        if (!$this->value) {

            return new Time(0, $this->unit);
        }

        return new Time($this->value, $this->unit);
    }
}
