<?php

namespace Berbass\QuantityBundle\Services\Providers;

use Money\Money;
use Berbass\QuantityBundle\Entity\MoneyData;
use Tbbc\MoneyBundle\Money\MoneyManager as BaseMoneyManager;
use Tbbc\MoneyBundle\Pair\PairManager;

/**
* Money manager to replace the one from TbbcMoneyBundle
*/
class MoneyManager extends BaseMoneyManager
{
	/**
     * @var PairManager
     */
    protected $pairManager;

	public function __construct(
        PairManager $pairManager,
        $referenceCurrencyCode,
        $decimals = 2
    )
    {
    	$this->pairManager = $pairManager;

        return parent::__construct($referenceCurrencyCode, $decimals);
    }

    public function balanceCurrencies(Money $val1, Money $val2, $currency = '')
    {
        if (!$currency) {
            
            $currency = $val1->getCurrency()->getName();
        }

        /** @var Money */
        $val1Converted = $this->pairManager->convert($val1, $currency);

        /** @var Money */
        $val2Converted = $this->pairManager->convert($val2, $currency);

        return [$val1Converted, $val2Converted];
    }

    /**
     * { function_description }
     *
     * @param \Money\Money $val1 The value 1
     * @param \Money\Money $val2 The value 2
     * @param string $currency The currency
     *
     * @return Money The monetary sum of $val1 and $val2
     */
    public function add(Money $val1, Money $val2, $currency = '')
    {
        $balancedValues = $this->balanceCurrencies($val1, $val2, $currency);

        return $balancedValues[0]->add($balancedValues[1]);
    }

    /**
     * { function_description }
     *
     * @param \Money\Money $val1 The value 1
     * @param \Money\Money $val2 The value 2
     * @param string $currency The currency
     *
     * @return Money The monetary sum of $val1 and $val2
     */
    public function subtract(Money $val1, Money $val2, $currency = '')
    {
        $balancedValues = $this->balanceCurrencies($val1, $val2, $currency);

        return $balancedValues[0]->subtract($balancedValues[1]);
    }

    public function multiply(Money $val, $multiplier, $roundingMode = Money::ROUND_HALF_UP)
    {
        return $val->multiply($multiplier, $roundingMode);
    }

    public function multiplyMD(MoneyData $val, $multiplier, $roundingMode = Money::ROUND_HALF_UP)
    {
        /** @var MoneyData */
        $out = clone $val;

        return $out->multiply($multiplier, $roundingMode);
    }

    public function divide(Money $val, $divisor, $roundingMode = Money::ROUND_HALF_UP)
    {
        return $val->divide($divisor, $roundingMode);
    }

    public function divideMD(MoneyData $val, $divisor, $roundingMode = Money::ROUND_HALF_UP)
    {
        /** @var MoneyData */
        $out = clone $val;

        return $out->divide($divisor, $roundingMode);
    }

    /**
     * Subs a md.
     *
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val1 The value 1
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val2 The value 2
     * @param string $currency The currency
     * @return MoneyData
     */
    public function subtractMD(MoneyData $val1, MoneyData $val2, $currency = '')
    {
        /** @var Money */
        $outPrice = $this->subtract($val1->getPrice(), $val2->getPrice(), $currency);

        return new MoneyData($outPrice->getAmount(), $outPrice->getCurrency()->getName());
    }

    /**
     * Adds a md.
     *
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val1 The value 1
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val2 The value 2
     * @param string $currency The currency
     * @return MoneyData
     */
    public function addMD(MoneyData $val1, MoneyData $val2, $currency = '')
    {
    	/** @var Money */
    	$outPrice = $this->add($val1->getPrice(), $val2->getPrice(), $currency);

    	return new MoneyData($outPrice->getAmount(), $outPrice->getCurrency()->getName());
    }

    public function getReferenceCurrency()
    {
    	return $this->getReferenceCurrency();
    }

    /**
     * @param \Money\Money $val1 The value 1
     * @param \Money\Money $val2 The value 2
     * @param string $currency The currency
     *
     * @return boolean
     */
    public function lessThan(Money $val1, Money $val2, $currency = '')
    {
        $balancedValues = $this->balanceCurrencies($val1, $val2, $currency);


        return $balancedValues[0]->lessThan($balancedValues[1]);
    }

    /**
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val1 The value 1
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val2 The value 2
     * @param string $currency The currency
     *
     * @return boolean
     */
    public function lessThanMD(MoneyData $val1, MoneyData $val2, $currency = '')
    {
        return $this->lessThan($val1->getPrice(), $val2->getPrice(), $currency);
    }

    /**
     * @param \Money\Money $val1 The value 1
     * @param \Money\Money $val2 The value 2
     * @param string $currency The currency
     *
     * @return boolean
     */
    public function greaterThan(Money $val1, Money $val2, $currency = '')
    {
        $balancedValues = $this->balanceCurrencies($val1, $val2, $currency);

        return $balancedValues[0]->greaterThan($balancedValues[1]);
    }

    /**
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val1 The value 1
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val2 The value 2
     * @param string $currency The currency
     *
     * @return boolean
     */
    public function greaterThanMD(MoneyData $val1, MoneyData $val2, $currency = '')
    {
        return $this->greaterThan($val1->getPrice(), $val2->getPrice(), $currency);
    }

    /**
     * @param \Money\Money $val1 The value 1
     * @param \Money\Money $val2 The value 2
     * @param string $currency The currency
     *
     * @return boolean 
     */
    public function equals(Money $val1, Money $val2, $currency = '')
    {
        $balancedValues = $this->balanceCurrencies($val1, $val2, $currency);

        return $balancedValues[0]->equals($balancedValues[1]);
    }

    /**
     * Determines if it equals md.
     *
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val1 The value 1
     * @param \Berbass\QuantityBundle\Entity\MoneyData $val2 The value 2
     * @param string $currency The currency
     *
     * @return <type> True if equals md, False otherwise.
     */
    public function equalsMD(MoneyData $val1, MoneyData $val2, $currency = '')
    {
        return $this->equals($val1->getPrice(), $val2->getPrice(), $currency);
    }
}