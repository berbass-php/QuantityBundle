<?php

namespace Berbass\QuantityBundle\Services\Formatters;

use Money\Currency;
use Money\Money;
use Tbbc\MoneyBundle\Formatter\MoneyFormatter as BaseMoneyFormatter;

/**
 * Money formatter
 */
class MoneyFormatter extends BaseMoneyFormatter
{
    /**
     * @var array
     */
    protected $decimalsMap;

    const FORMAT_PART_AMOUNT = 'amount';
    const FORMAT_PART_CURRENCY = 'currency';

    public function __construct($decimals = 2, $decimalsMap = [])
    {
        $this->decimalsMap = $decimalsMap;

        return parent::__construct($decimals);
    }

    public function formatPart($part, Money $money, $decPoint = ',', $thousandsSep = ' ')
    {
        if (!in_array($part, [self::FORMAT_PART_AMOUNT, self::FORMAT_PART_CURRENCY])) {
            
            throw new \UnexpectedValueException("The requested format part is not valid", 1);
        }

        switch ($part) {
            case self::FORMAT_PART_AMOUNT:

                return $this->formatAmount($money, $decPoint, $thousandsSep);
            
            case self::FORMAT_PART_CURRENCY:

                return $this->formatCurrency($money, $decPoint, $thousandsSep);
        }
    }

    /**
     * Formats the given Money object
     * INCLUDING the currency symbol
     *
     * @param Money  $money
     * @param string $decPoint
     * @param string $thousandsSep
     *
     * @return string
     */
    public function formatMoney(Money $money, $decPoint = ',', $thousandsSep = ' ')
    {
        $symbol = $this->formatCurrency($money);
        $amount = $this->formatAmount($money, $decPoint, $thousandsSep);
        $price = $symbol . " " . $amount;

        return $price;
    }

    /**
     * Formats the amount part of the given Money object
     * WITHOUT INCLUDING the currency symbol
     *
     * @param Money  $money
     * @param string $decPoint
     * @param string $thousandsSep
     *
     * @return string
     */
    public function formatAmount(Money $money, $decPoint = ',', $thousandsSep = ' ')
    {
        $amount = $this->asFloat($money);
        $currency = $money->getCurrency()->getName();

        $decimals = isset($this->decimalsMap[$currency]) ? $this->decimalsMap[$currency] : $this->decimals;


        $amount = number_format($amount, $decimals, $decPoint, $thousandsSep);

        return $amount;
    }

    /**
     * Gets the decimals for currency.
     *
     * @param string $currency The currency
     *
     * @return int The decimals for currency.
     */
    public function getDecimalsForCurrency($currency)
    {
        return empty($this->decimalsMap[$currency])
            ? $this->decimals
            : $this->decimalsMap[$currency]
        ;
    }
}
