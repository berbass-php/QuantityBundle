<?php

namespace Berbass\QuantityBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @Annotation
 */
class MassDataRange extends Constraint
{
    public $message = 'Les valeurs valides sont entre (%min%) et (%max%).';
    public $minMessage = 'La valeur du champs "%field%" doit être égale à %min% ou plus.';
    public $maxMessage = 'La valeur du champs "%field%" doit être égale à %max% ou moins.';
    public $min;
    public $max;
    public $field = 'weight';
    public $fieldLabel;

    public function validatedBy()
	{
		return 'validator.mass_data_range';
	}

    /**
     * {@inheritDoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function __construct($options = null)
    {
        parent::__construct($options);

        if (null === $this->min && null === $this->max) {
            throw new MissingOptionsException(sprintf('Either option "min" or "max" must be given for constraint %s', __CLASS__), array('min', 'max'));
        }
    }
}
