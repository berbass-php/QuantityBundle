<?php

namespace Berbass\QuantityBundle\Validator\Constraints;

use Berbass\QuantityBundle\Entity\MassData;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class MassDataRangeValidator extends ConstraintValidator
{
    /**
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        if (!$constraint instanceof MassDataRange) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Range');
        }

        /** @var PropertyAccessorInterface */
        $accessor = PropertyAccess::createPropertyAccessorBuilder()->enableMagicCall()->getPropertyAccessor();

        /** @var MassData|mixed */
        $weight = $accessor->getValue($object, $constraint->field);

        /** @var MassData|NULL */
        $minWeight = $constraint->min
            ? $this->buildMassFromString($constraint->min)
            : NULL
        ;

        /** @var MassData|NULL */
        $maxWeight = $constraint->max
            ? $this->buildMassFromString($constraint->max)
            : NULL
        ;

        if (
            $minWeight
            && $weight->getQuantity()->subtract($minWeight->getQuantity())->toNativeUnit() < 0
        ) {

            $this->context->buildViolation($constraint->minMessage)
                ->setParameter('%min%', (string) $minWeight)
                ->setParameter('%field%', $constraint->fieldLabel ?: $constraint->field)
                ->atPath($constraint->field)
                ->addViolation()
            ;
        }

        if (
            $maxWeight
            && $maxWeight->getQuantity()->subtract($weight->getQuantity())->toNativeUnit() < 0
        ) {

            $this->context->buildViolation($constraint->maxMessage)
                ->setParameter('%max%', (string) $maxWeight)
                ->setParameter('%field%', $constraint->fieldLabel ?: $constraint->field)
                ->atPath($constraint->field)
                ->addViolation()
            ;
        }
    }

    /**
     * Builds a mass from string.
     *
     * @param <type> $value The value
     *
     * @return MassData The mass from string.
     */
    protected function buildMassFromString($value)
    {
        $data = explode(' ', $value);

        /** MassData */
        $out = new MassData(
            isset($data[0]) ? $data[0] : 0,
            isset($data[1]) ? $data[1] : NULL
        );

        return $out;
    }
}
