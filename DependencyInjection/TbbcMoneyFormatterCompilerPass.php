<?php

namespace Berbass\QuantityBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Parameter;

/**
 * Class for entity extend builder compiler pass.
 * 
 * To be used for OroPlatform version 1.10.1
 * If any update of OroPlatform is made, check this pass to see if it is still needed
 */
class TbbcMoneyFormatterCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('tbbc_money.formatter.money_formatter');

        $definition->setClass('Berbass\QuantityBundle\Services\Formatters\MoneyFormatter');

        $definition->setArguments([
        	new Parameter('tbbc_money.decimals'),
        	new Parameter('symdrik_quantity.money.decimals_map')
        ]);
    }
}