<?php

namespace Berbass\QuantityBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;

class TbbcMoneyTypeCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('tbbc_money.form_type.money');
        $definition->setClass('Berbass\QuantityBundle\Form\Type\TbbcMoneyType');

        $moneyValueScale = NULL;

        if ($container->hasParameter('money_field.value_scale')) {
        	
        	$moneyValueScale = $container->getParameter('money_field.value_scale');
        } else {

            throw new ParameterNotFoundException('money_field.value_scale');
        }

        $definition->addArgument($moneyValueScale);
    }
}