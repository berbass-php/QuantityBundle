<?php

namespace Berbass\QuantityBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Berbass\QuantityBundle\Utils\MoneyUtil;

class TbbcCurrencyTypeCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('tbbc_money.form_type.currency');

        $currencies = $container->getParameterBag()
        	->resolveValue($definition->getArgument(0));

        if (is_array($currencies) && $currencies[0] == '*') {


        	$definition->setArguments([
        		array_keys(MoneyUtil::getCurrenciesList()),
        		$container->getParameterBag()->resolveValue('%tbbc_money.reference_currency%')
        	]);
        }
    }
}