<?php

namespace Berbass\QuantityBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class for entity extend builder compiler pass.
 * 
 * To be used for OroPlatform version 1.10.1
 * If any update of OroPlatform is made, check this pass to see if it is still needed
 */
class TbbcMoneyManagerCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
    	/** @var Definition */
        $definition = $container->getDefinition('tbbc_money.money_manager');

        $definition->setClass('Berbass\QuantityBundle\Services\Providers\MoneyManager');

        $definition->setArguments([
        	new Reference('tbbc_money.pair_manager'),
        	new Parameter('tbbc_money.reference_currency'),
        	new Parameter('tbbc_money.decimals')
        ]);
    }
}