<?php

namespace Berbass\QuantityBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class MoneyPairManagerCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('tbbc_money.pair_manager');
        $definition->setClass('Berbass\QuantityBundle\Model\Money\PairManager');
    }
}